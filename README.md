# ***Projeto Landing Page***

## 📃 Introdução

Eae Pedro, tudo na paz?
<br>
Neste projeto foi utilizado duas bibliotecas open-source, elas são: 
 
- [AOS scroll library](https://michalsnik.github.io/aos/)
- [SwiperJs](https://swiperjs.com/)

Com elas pude aprender como utilizar de bibliotecas para agilizar o processo de criação de site. Além disso, aprendi coisas novas relacionadas a elas, e com isso, posso aplicar esse novo aprendizado nos proximos projetos que eu for desenvolver. 
<br>

*Ademais, a página está totalmente responsiva*📱
<br>

Contudo, nesse projeto possue dois problemas, que são:
<br>

- Na section do pricing, não consegui animar o botão e como consequência essa parte ficou estática

- quando a página for adaptada para dispositivo mobile, há um problema de escala. A página fica um pouco maior que necessário e [clicando nesse link você entenderá melhor](https://gyazo.com/9e2580f4aae7675587cf35f29212fbb0). Por algum motivo para página voltar ao normal, basta só clicar para expandir as perguntas do FAQ.

✨*Um addendum*: Tem uma Easter-Egg no swiper da section Testiminial 


## ***Espero que goste*** 😉
